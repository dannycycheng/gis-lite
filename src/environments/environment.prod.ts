export const environment = {
  production: true,
  epsg4326: "EPSG:4326",
  epsg3857: "EPSG:3857",
  mapboxApiKey: "pk.eyJ1IjoibGF1Z2hvdXRsb3VkIiwiYSI6ImNqaXdtZjB3NDJlZ3Aza21zYTV6bDd1a3oifQ.2FfDssjvsPRaPGJq9OxcwA"
};
