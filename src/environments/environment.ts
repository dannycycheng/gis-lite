// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  epsg4326: "EPSG:4326",
  epsg3857: "EPSG:3857",
  mapboxApiKey: "pk.eyJ1IjoibGF1Z2hvdXRsb3VkIiwiYSI6ImNqaXdtZjB3NDJlZ3Aza21zYTV6bDd1a3oifQ.2FfDssjvsPRaPGJq9OxcwA"
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
