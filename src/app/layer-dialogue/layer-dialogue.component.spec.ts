import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayerDialogueComponent } from './layer-dialogue.component';

describe('LayerPanelComponent', () => {
  let component: LayerDialogueComponent;
  let fixture: ComponentFixture<LayerDialogueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayerDialogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayerDialogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
