import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import WMSCapabilities from 'ol/format/WMSCapabilities';

import { MapService } from '../../map/map.service';
import { LayerInfo } from '../../map/layer-info';

@Component({
  selector: 'add-layer',
  templateUrl: './add-layer.component.html',
  styleUrls: ['./add-layer.component.css']
})
export class AddLayerComponent implements OnInit {

  @Input()
  private display: boolean;

  @Output()
  private onClose: EventEmitter<boolean> = new EventEmitter<boolean>();

  private layerName: string;
  private selectedLayers: Array<Object>;
  private getCapabilitiesUrl: string;
  private availableLayers: Array<Object>;

  constructor(private mapService: MapService) { }

  ngOnInit() {
  }

  private loadAvailableLayers(capabilities: string) {
    let parser = new WMSCapabilities();
    let wmsCapabilities = parser.read(capabilities);
    this.availableLayers = wmsCapabilities.Capability.Layer.Layer;
  }

  private checkGetCapabilities(event: any) {
    this.mapService.getLayersFromCapabilities(this.getCapabilitiesUrl).subscribe(
      (data) => this.loadAvailableLayers(data),
      (error) => console.log('Search returned an error, do something about it!')
    );
  }

  private onHide() {
    this.onClose.emit(true);
  }

  private onSave() {
    console.log(this.selectedLayers);
    this.display = false;

    let layersToAdd = new Array<LayerInfo>();
    for (let layer of this.selectedLayers) {
      let layerInfo: LayerInfo = {
        baseUrl: 'string',
        layerName: 'name'
      }

      layersToAdd.push(layerInfo);
    }

    this.mapService.addLayers(layersToAdd);

    // make sure the form is clean the next time it's opened
    this.clearForm();
  }

  private clearForm() {
    this.layerName = '';
    this.getCapabilitiesUrl = '';
    this.selectedLayers = [];
    this.availableLayers = [];
  }
}
