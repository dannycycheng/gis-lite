import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'layer-dialogue',
  templateUrl: './layer-dialogue.component.html',
  styleUrls: ['./layer-dialogue.component.css']
})
export class LayerDialogueComponent implements OnInit {

  @Input()
  private display: boolean;
  
  @Output()
  private onClose: EventEmitter<boolean> = new EventEmitter<boolean>();

  private showAddLayer: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  private toggleAddLayerDialogue() {
    this.showAddLayer = !this.showAddLayer;
  }

  private onHide() {
    this.onClose.emit(true);
  }

  private addLayerClosed() {
    this.showAddLayer = false;
  }
}
