export interface GeocodeDataFeatureContext {
    id: string;
    short_code: string;
    wikidata: string;
    text: string;
}

export interface GeocodeDataFeature {
    id: string;
    type: string;
    place_type: string[];
    relevance: number;
    properties: any;
    text: string;
    place_name: string;
    bbox: number[]; // minX, minY, maxX, maxY
    center: number[]; // x, y
    geometry: {
        type: string;
        coordinates: number[];
    };
    context: GeocodeDataFeatureContext[];
}

export interface GeocodeData {
    type: string;
    query: any[];
    features: GeocodeDataFeature[];
    attribution: string;
}