import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

import { environment } from '@env/environment';
import { GeocodeData } from './geocode-data';

@Injectable({
  providedIn: 'root'
})
export class MapboxService {

  private apiToken = environment.mapboxApiKey;
  private baseUrl = "https://api.mapbox.com";

  constructor(private http: HttpClient) { }

  public forwardSearch(searchString: string) {
    let uri = `/geocoding/v5/mapbox.places/${searchString}.json?access_token=${this.apiToken}`;
    return this.http.get<GeocodeData>(this.baseUrl + uri).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  public reverseSearch(lat: string, lon: string) {
    let uri = `/geocoding/v5/mapbox.places/${lon},${lat}.json?access_token=${this.apiToken}`;
    return this.http.get<GeocodeData>(this.baseUrl + uri).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  private handleError(response: HttpErrorResponse) {
    console.log("Error handler: " + response.error);
    return throwError("Something bad happened; please try again later.");
  }
}
