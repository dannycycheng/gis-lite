import { Component, OnInit } from '@angular/core';

import { MapboxService } from './mapbox.service';
import { MapService } from '../map/map.service';
import { GeocodeData } from './geocode-data';

export interface SearchResult {
  name: string;
  centre: number[]; // x,y
  bbox: number[]; // minX,minY,maxX,maxY
}

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  private searchResults: SearchResult[] = new Array<SearchResult>();
  private searchString: string;

  constructor(private mapboxService: MapboxService, private mapService: MapService) { }

  public ngOnInit() { }

  private search(event: any) {
    // only search if the input is non-empty
    if (event.query !== undefined && event.query.length !== 0) {
      this.mapboxService.forwardSearch(event.query).subscribe(
        (data) => this.onSearch(data),
        (error) => console.log('Search returned an error, do something about it!')
      );
    }
  }

  private onSearch(result: GeocodeData) {
    /* IMPORTANT PrimeNG API Note.
    AutoComplete either uses setter based checking or ngDoCheck
    to realize if the suggestions has changed to update the UI.
    This is configured using the immutable property, when enabled (default)
    setter based detection is utilized so your changes such as
    adding or removing a record should always create a new array
    reference instead of manipulating an existing array as Angular
    does not trigger setters if the reference does not change.
    For example, use slice instead of splice when removing an item
    or use spread operator instead of push method when adding an item.
    On the other hand, setting immutable property to false removes
    this restriction by using ngDoCheck with IterableDiffers to listen
    changes without the need to create a new reference of data. Setter
    based method is faster however both methods can be used
    depending on your preference.

    Note that if no suggestions are available after searching,
    provide an empty array instead of a null value.
    */
    this.searchResults = []; // clear old results

    for (let feature of result.features) {
      let tempResult = {
        name: feature.place_name,
        centre: feature.center,
        bbox: feature.bbox
      };
      this.searchResults.push(tempResult);
    }
  }

  private onSelect(selection: any) {
    if (selection.bbox === undefined) {
      this.mapService.goToLocation(selection.centre);
    }
    else {
      this.mapService.goToLocation(selection.bbox);
    }
  }
}
