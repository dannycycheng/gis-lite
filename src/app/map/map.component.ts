import { Component, OnInit } from '@angular/core';
import { Map, View } from 'ol';
import OSM from 'ol/source/OSM';
import TileLayer from 'ol/layer/Tile';
import TileWMS from 'ol/source/TileWMS';
import { fromLonLat, transformExtent } from 'ol/proj';
import { ScaleLine, defaults as defaultControl } from 'ol/control';
import MousePosition from 'ol/control/MousePosition';
import { boundingExtent } from 'ol/extent';

import { MapService } from './map.service';
import { environment } from '@env/environment';

@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  // default map setup
  private defaultProjection = environment.epsg3857;
  private defaultZoom = { min: 3, max: 19 };
  private defaultBasemap = new TileLayer({
    source: new OSM()
  });
  private defaultControl = defaultControl().extend([
    new ScaleLine(),
    new MousePosition({ projection: environment.epsg4326 })
  ]);
  private defaultView = new View({
    projection: this.defaultProjection,
    center: fromLonLat([-122.999667, 49.226915]),
    minZoom: this.defaultZoom.min,
    maxZoom: this.defaultZoom.max,
    zoom: 15
  });

  // private MapComponent members
  private map;
  private layers;

  constructor(private mapService: MapService) { }

  ngOnInit() {
    this.layers = [this.defaultBasemap];

    this.map = new Map({
      target: 'map',
      controls: this.defaultControl,
      layers: this.layers,
      view: this.defaultView
    });

    // subscribe to map events
    this.mapService.locationChanged$.subscribe(
      (location) => this.zoomToExtent(location, true)
    )

    this.mapService.layersAdded$.subscribe(
      (layers) => this.addLayers(layers)
    )
  }

  /**
   * Zoom the map to the provided extent
   * 
   * @param coords the extent to zoom to
   * @param transform transform from EPSG:4326 to EPSG:3857
   * @param maxZoom the max zoom level to zoom to
   */
  private zoomToExtent(coords: Array<Array<number>>, transform?: boolean, maxZoom?: number) {
    let extent = boundingExtent(coords);
    if (transform) {
      extent = transformExtent(extent, environment.epsg4326, environment.epsg3857);
    }

    this.map.getView().fit(extent, { maxZoom: maxZoom });
  }

  private addLayers(layers: any[]) {
    console.log(layers);
    for (let layer of layers) {
      let wmsSource = new TileWMS({
        params: {
          'LAYERS': layer.Name,
          'TILED': true
        },
        url: "http://maps.geogratis.gc.ca/wms/roads_en"
      });
  
      let layerToAdd = new TileLayer({
        source: wmsSource
      });

      this.map.addLayer(layerToAdd);
    }
  }
}
