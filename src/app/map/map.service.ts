import { Injectable } from '@angular/core';
import { retry, catchError, map } from 'rxjs/operators';
import { Subject, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  private locationSubject = new Subject<any>();
  public locationChanged$ = this.locationSubject.asObservable();

  private addLayersSubject = new Subject<any[]>();
  public layersAdded$ = this.addLayersSubject.asObservable();

  constructor(private http: HttpClient) { }

  public goToLocation(location: number[]) {
    let olCoordinates = [];
    for (let index = 0; index + 1 < location.length; index += 2) {
      olCoordinates.push([location[index], location[index + 1]])
    }
    this.locationSubject.next(olCoordinates);
  }

  public addLayers(layers: Object[]) {
    this.addLayersSubject.next(layers);
  }

  public getLayersFromCapabilities(url: string) {
    const options = {
      headers: new HttpHeaders(),
      responseType: 'text' as 'text'
    };

    return this.http.get(url, options).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  private handleError(response: HttpErrorResponse) {
    console.log("Error handler: " + response.error);
    return throwError("Something bad happened; please try again later.");
  }
}
