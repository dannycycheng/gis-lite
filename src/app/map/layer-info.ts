export interface LayerInfo {
    baseUrl: string;
    layerName: string;
}