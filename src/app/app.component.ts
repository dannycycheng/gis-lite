import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  private showLayerDialogue: boolean = false;

  constructor() { }

  private layerDialogueClosed() {
    this.showLayerDialogue = false;
  }

  private toggleLayerDialogue() {
    this.showLayerDialogue = !this.showLayerDialogue;
  }
}
